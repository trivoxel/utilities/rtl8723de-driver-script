#!/bin/bash
# Script written by TriVoxel (https://TriVoxel.page.link/site)
# Code taken from https://ubuntuhandbook.org/index.php/2018/no-wifi-adapter-found-hp-laptops-ubuntu-18-04/
# Feel free to modify any part of this script and re-distribute it.

## Kill program on any errors
set -e

## If this isn't being run as root, request it
[ $(id -u) -ne 0 ] && exec sudo $0 $SUDOARGS

## Install dependencies for Ubuntu/Debian (will require an ethernet connection or programs to be pre-installed)
## This can be done via another ethernet-capable device with Linux with WiFi sharing enabled.
## See https://askubuntu.com/questions/359856/ for detailed instructions with `nm-applet`
sudo apt-get install libelf-dev linux-headers-$(uname -r) build-essential git

## Main operations
echo "Cloning repo..."
## We skip re-cloning the repo here
if [ ! -d /tmp/rtlwifi_new/ ]; then
	git clone https://github.com/lwfinger/rtlwifi_new.git /tmp/rtlwifi_new/
	echo "Entering source directory..." && sleep 1
	cd /tmp/rtlwifi_new/ && git checkout origin/extended -b extended
else
	echo "Git files already exist on drive. Skipping git clone..." && sleep 5
	cd /tmp/rtlwifi_new/
fi
echo "Install modules..." && sleep 1
sudo make install
echo "Probing kernel modules..." && sleep 1
sudo modprobe -r rtl8723de && sudo modprobe rtl8723de
echo "Attempt to purge possible conflicting package..." && sleep 1
sudo apt purge bcmwl-kernel-source
echo "Remove blacklists..." && sleep 1
sudo sed -i '/blacklist bcma/ d' /etc/modprobe.d/blacklist.conf && sudo sed -i '/blacklist brcmsmac/ d' /etc/modprobe.d/blacklist.conf
echo "Probe kernel modules once more..." && sleep 1
sudo modprobe -r rtl8723de && echo "Select different antenna? (Y/n)"
read poll

## Antenna may be incorrect by default for some users so we do this
if [ $poll = "Y" ]; then
	echo "Please type value of 1-3 for antenna channel:" && sleep 1
	read ant
	sudo modprobe rtl8723de ant_sel=$ant
	echo "Channel $ant selected." && sleep 1
else
	echo "Skipping antenna change..."
	echo "Finalizing kernel module probes..." && sleep 1
	sudo modprobe rtl8723de
fi

## I'm a friendly coder... sometimes!
echo "Operations complete! Restart? (Y/n)"
read poll
if [[ $poll = Y ]]; then
	reboot
	exit
fi
echo -e "Skipping reboot. \nYou may now close this terminal!"
exit
