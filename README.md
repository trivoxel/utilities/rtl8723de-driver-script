# rtl8723de Driver Auto-Installer
## By [TriVoxel](https://TriVoxel.page.link/Site) (Caden Mitchell)

## [Download](https://gitlab.com/TriVoxel/rtl8723de-driver-script/raw/master/rtl8723de_Driver_Auto-Installer.sh?inline=false)

### Fully-automatic RTL8623DE driver install script for Linux
An automated way of installing the Realtek wireless driver on Linux systems. Made due to a need to automate this for my HP laptop. Tested and working with Arch Linux and Ubuntu 18.04+. Uses [this post](https://askubuntu.com/questions/983251) and [this post](https://h30434.www3.hp.com/t5/Notebook-Wireless-and-Networking/Realtek-8723DE-wifi-module-amp-Bluetooth-Linux-driver/td-p/6477307) and turns it into an automated script.

### Which one do I use?
The [download](https://gitlab.com/TriVoxel/rtl8723de-driver-script/raw/master/rtl8723de_Driver_Auto-Installer.sh?inline=false) link will give the the recommended one (`rtl8723de_Driver_Auto-Installer.sh`). The `rtl8723de_Driver_Auto-Installer.sh` script is the curret version with all the features and improvements. It is based on `Old_rtl8723de_Driver_Auto-Installer.sh` which is a complete refactoring of `Bad_rtl8723de_Driver_Auto-Installer.sh` with many improvements. `Bad_rtl8723de_Driver_Auto-Installer.sh` is a completely different method that requires probing the WiFi driver manually after every reboot and suspend. It also gives poor connection and range. TL:DR Don't use `Bad_rtl8723de_Driver_Auto-Installer.sh`, use `rtl8723de_Driver_Auto-Installer.sh`! :P

I recommend using an ethernet connection to setup. You can use another device with ethernet and an internet connection to connect your laptop to the internet while doing this. More info [here](https://askubuntu.com/questions/359856/). Antenna `2` and `3` work best so the defualt is 2.

### Full offline support
This project supports offline functionality with the `--offline` flag set. To use this, you will need to preinstall `linux-headers` (for your respective system), `dkms`. There is not really an easy way to do this unfortunately. I may make a tool to do this in the future. You will need to use another computer to download [this source code](https://github.com/lwfinger/rtlwifi_new.git) ("extended" branch) and place the extracted folder in `/tmp/rtl_wifi/` on your offline system. For Bluetooth, you will need to download [this source code](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git) ("master" branch) and place it in `/tmp/rtl_bt`.

### Limitations
Currently there are only configurations for the Debian [Apt](https://en.wikipedia.org/wiki/APT_(software)) and Arch Linux [Pacman](https://wiki.archlinux.org/index.php/pacman). I may add support for more Linux package managers in the future. Another limitation is the lack of specifying the source folder. There were too many problems with my current setup of it so I've slated that feature for the time being. Maybe in the future I can also just port this to C and bundle it with the dependencies to eliminate downloading the additional packages. For now this works. Please direct any DKMS-specific questions towards the forum for your operating system and link this script to them so they can understand how to help you. Any problems related to other parts of this software can be placed in an [issue](https://gitlab.com/TriVoxel/rtl8723de-driver-script/issues).

**Make sure to visit [my website](https://TriVoxel.page.link/site)!**
